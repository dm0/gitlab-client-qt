# GitLab API Client in C++ (Qt)

The goal of this is to create GitLab API client library to use it in [GitLab Tracker](https://gitlab.com/dm0/gitlab-tracker) project.  
The library is implemented using Qt framework and exposes `signals` and `slots` to provide asynchronous behavior. It supports oAuth grant flow to authenticate with GitLab or can use existing private token.

<!--@m_class{m-block m-info} @-->

<!--@parblock @--> <h3>Limitations</h3>
At the moment only a limited set of GitLab API planned for implementation.  
In particular only the API required to implement [GitLab Tracker](https://gitlab.com/dm0/gitlab-tracker) desktop application is implemented.

<!--@endparblock @-->

<!--@m_div{m-button m-success} @-->
[Continue to API documentation](https://dm0.gitlab.io/gitlab-client-qt/annotated.html) 
<!--@m_enddiv @-->