TEMPLATE = lib

QT += gui network networkauth

CONFIG += c++11 create_prl
CONFIG -= app_bundle

gitlab_client_qt_static {
    CONFIG += static
}

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $${PWD}/include/gitlabclient

SOURCES += \
    src/gitlabclient.cpp \
    src/projects.cpp \
    src/asyncjsonresponse.cpp \
    src/asyncjsonobjlist.cpp \
    src/asyncjsonobject.cpp \
    src/issues.cpp \
    src/queryparams.cpp \
    src/notes.cpp \
    src/mreqs.cpp

HEADERS += \
    include/gitlabclient/gitlabclient \
    include/gitlabclient/gitlabclient.h \
    include/gitlabclient/projects.h \
    include/gitlabclient/asyncjsonresponse.h \
    include/gitlabclient/asyncjsonobjlist.h \
    include/gitlabclient/asyncjsonobject.h \
    include/gitlabclient/issues.h \
    include/gitlabclient/queryparams.h \
    include/gitlabclient/notes.h \
    include/gitlabclient/mreqs.h


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /usr/lib
!gitlab_client_qt_noinstall:!isEmpty(target.path): INSTALLS += target


include(third_parties/qmakescm/git.pri)
