#include <QtNetworkAuth>

#include <QDebug>

#include "notes.h"

using namespace gitlabclient;

Notes::Notes(gitlabclient::Client *client):
    QObject(client),
    client(client)
{

}

AsyncJsonObjList * Notes::list_issue_notes(const QString &project_path, long iid, bool fetch_all)
{
    static const QStringList known_params{"order_by", "sort"};
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/issues/" % QString::number(iid) %
                     "/notes");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Notes::list_issue_notes(long project_id, long iid, bool fetch_all)
{
    return list_issue_notes(QString::number(project_id), iid, fetch_all);
}

AsyncJsonObject * Notes::get_issue_note(const QString &project_path, long iid, long nid)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/issues/" % QString::number(iid) %
                     "/notes/" % QString::number(nid));

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response = client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * Notes::get_issue_note(long project_id, long iid, long nid)
{
    return get_issue_note(QString::number(project_id), iid, nid);
}

AsyncJsonObjList * Notes::list_mr_notes(const QString &project_path_or_id, long iid, bool fetch_all)
{
    static const QStringList known_params{"order_by", "sort"};
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path_or_id) %
                     "/merge_requests/" % QString::number(iid) %
                     "/notes");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Notes::list_mr_notes(long project_id, long iid, bool fetch_all)
{
    return list_mr_notes(QString::number(project_id), iid, fetch_all);
}

AsyncJsonObject * Notes::get_mr_note(const QString &project_path, long iid, long nid)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/merge_requests/" % QString::number(iid) %
                     "/notes/" % QString::number(nid));

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response = client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * Notes::get_mr_note(long project_id, long iid, long nid)
{
    return get_mr_note(QString::number(project_id), iid, nid);
}
