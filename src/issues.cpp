#include <QtNetworkAuth>

#include <QDebug>

#include "issues.h"

using namespace gitlabclient;

Issues::Issues(Client *client):
    QObject(client),
    client(client)
{

}

AsyncJsonObjList * Issues::list_issues(bool fetch_all)
{
    static const QStringList known_params{
        "state", "labels", "milestone", "scope", "author_id", "assignee_id", "my_reaction_emoji",
        "weight", "order_by", "sort", "search", "in", "created_after",
        "created_before", "updated_after", "updated_before"
    };
    QUrl request_url(client->api_endpoint() %
                     "/issues");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Issues::list_group_issues(const QString &path, bool fetch_all)
{
    static const QStringList known_params{
        "state", "labels", "milestone", "scope", "author_id", "assignee_id", "my_reaction_emoji",
        "weight", "order_by", "sort", "search", "in", "created_after",
        "created_before", "updated_after", "updated_before"
    };
    QUrl request_url(client->api_endpoint() %
                     "/groups/" % QUrl::toPercentEncoding(path) %
                     "/issues");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Issues::list_group_issues(long id, bool fetch_all)
{
    return list_group_issues(QString::number(id), fetch_all);
}


AsyncJsonObjList * Issues::list_project_issues(const QString &path, bool fetch_all)
{
    static const QStringList known_params{
        "state", "labels", "milestone", "scope", "author_id", "assignee_id", "my_reaction_emoji",
        "weight", "order_by", "sort", "search", "in", "created_after",
        "created_before", "updated_after", "updated_before"
    };
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(path) %
                     "/issues");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Issues::list_project_issues(long id, bool fetch_all)
{
    return list_project_issues(QString::number(id), fetch_all);
}

AsyncJsonObject * Issues::get_issue(const QString &project_path, long issue_id)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/issues/" % QString::number(issue_id));

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response = client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}


AsyncJsonObject * Issues::get_issue(long project_id, long issue_id)
{
    return get_issue(QString::number(project_id), issue_id);
}


AsyncJsonObject * Issues::set_time_estimate(const QString &project_path_or_id, long issue_id,
                                            const QString &time_estimate)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path_or_id) %
                     "/issues/" % QString::number(issue_id) %
                     "/time_estimate");

    qDebug() << "Sending request to " << request_url << "duration:" << time_estimate;
    QNetworkReply *response =
        client->auth_provider()->post(request_url, {{"duration", time_estimate}});
    return new AsyncJsonObject(response, client);
}


AsyncJsonObject * Issues::set_time_estimate(long project_id, long issue_id,
                                            const QString &time_estimate)
{
    return set_time_estimate(QString::number(project_id), issue_id, time_estimate);
}


AsyncJsonObject * Issues::get_time_stats(const QString &project_path, long issue_id)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/issues/" % QString::number(issue_id) %
                     "/time_stats");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response = client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}


AsyncJsonObject * Issues::get_time_stats(long project_id, long issue_id)
{
    return get_time_stats(QString::number(project_id), issue_id);
}


AsyncJsonObject * Issues::add_spent_time(const QString &project_path, long issue_id,
                                         const QString &spent_time)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/issues/" % QString::number(issue_id) %
                     "/add_spent_time");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->post(request_url, {{"duration", spent_time}});
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * Issues::add_spent_time(long project_id, long issue_id,
                                         const QString &spent_time)
{
    return add_spent_time(QString::number(project_id), issue_id, spent_time);
}
