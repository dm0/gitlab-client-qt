#include "queryparams.h"

using namespace gitlabclient;

QVariantMap QueryParams::get_valid(const QStringList &valid_names) const
{
    QVariantMap dst;
    const QVariantMap::const_iterator const_end = params.constEnd();
    for (const QString &name: valid_names) {
        auto it = params.constFind(name);
        if (it != const_end && it.value().isValid()) {
            dst[name] = it.value();
        }
    }
    return dst;
}
