#include <QtNetworkAuth>

#include <QDebug>

#include "asyncjsonresponse.h"

using namespace gitlabclient;

// Error reporting facilities.
namespace { // anonymous namespace
struct AsyncJsonResponseErrCategory: std::error_category {
    const char * name() const noexcept override;
    std::string message(int ev) const override;
};

const char * AsyncJsonResponseErrCategory::name() const noexcept
{
    return "async-json-response";
}

std::string AsyncJsonResponseErrCategory::message(int ev) const
{
    using Error = gitlabclient::AsyncJsonResponse::Error;
    switch (static_cast<Error>(ev)) {
        case Error::NoError:
            return "(no error)";

        case Error::Pending:
            return "another request is already in progress";

        case Error::NoMorePages:
            return "no more pages available";
    }
    return "(unrecognized error)";
}

// the suppressed warning is actually a false positive. While the type is non-POD, it has a
// constexpr constructor.
const AsyncJsonResponseErrCategory err_category; // clazy:exclude=non-pod-global-static
} // anonymous namespace

std::error_code gitlabclient::make_error_code(AsyncJsonResponse::Error e)
{
    return {static_cast<int>(e), err_category};
}

AsyncJsonResponse::AsyncJsonResponse(QNetworkReply *reply, Client *client):
    QObject(client),
    client(client)
{
    source_url = reply->request().url();
    attach_reply(reply);
}

std::error_code AsyncJsonResponse::refresh()
{
    attach_reply(client->auth_provider()->get(source_url));
    return Error::NoError;
}

bool AsyncJsonResponse::has_more() const
{
    return !next_page_link.isNull();
}

std::error_code AsyncJsonResponse::fetch_next()
{
    if (next_page_link.isNull()) {
        return Error::NoMorePages;
    }
    attach_reply(client->auth_provider()->get(next_page_link));
    return Error::NoError;
}

void AsyncJsonResponse::reply_finished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (!reply) {
        return;
    }
    process_reply(reply);
    reply->deleteLater();
    fetched(json_document);
    emit finished(last_error);
}

void AsyncJsonResponse::reply_error(QNetworkReply::NetworkError error)
{
    last_error = error;
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if (reply) {
        process_reply(reply);
    }
    fetch_failed(error);
}

void AsyncJsonResponse::fetched(const QJsonDocument & /*json*/)
{
    if (last_error == QNetworkReply::NoError) {
        emit loaded(current_page_num, total_pages_cnt);
    }
}

void AsyncJsonResponse::fetch_failed(QNetworkReply::NetworkError error)
{
    emit failed(error);
}

void AsyncJsonResponse::attach_reply(QNetworkReply *reply)
{
    using err_slot_t = void (QNetworkReply::*)(QNetworkReply::NetworkError);
    connect(reply, &QNetworkReply::finished, this, &AsyncJsonResponse::reply_finished);
    connect(reply, static_cast<err_slot_t>(&QNetworkReply::error),
            this, &AsyncJsonResponse::reply_error);

    has_active_request = true;
}

void AsyncJsonResponse::process_reply(QNetworkReply *reply)
{
    has_active_request = false;

    // set reply to be deleted later
    reply->deleteLater();

    json_document = QJsonDocument::fromJson(reply->readAll());
    last_error = reply->error();

    // Parse headers
    if (reply->hasRawHeader("Date")) {
        when = QDateTime::fromString(reply->rawHeader("Date"), Qt::RFC2822Date);
    }
    if (reply->hasRawHeader("X-Total")) {
        total_results_cnt = reply->rawHeader("X-Total").toInt();
    }
    if (reply->hasRawHeader("X-Total-Pages")) {
        total_pages_cnt = reply->rawHeader("X-Total-Pages").toInt();
    }
    if (reply->hasRawHeader("X-Page")) {
        current_page_num = reply->rawHeader("X-Page").toInt();
    }
    if (reply->hasRawHeader("Link")) {
        QRegExp rx_next(R"/(<([^>]+)>;\s+rel="next")/");
        int pos = rx_next.indexIn(reply->rawHeader("Link"));
        next_page_link = pos > -1 ? rx_next.cap(1) : QString();
    }
}


