#include <QtNetworkAuth>

#include <QDebug>

#include "mreqs.h"

using namespace gitlabclient;

MergeRequests::MergeRequests(Client *client):
    QObject(client),
    client(client)
{

}

AsyncJsonObjList * MergeRequests::list_mreqs(bool fetch_all)
{
    static const QStringList known_params{
        "state", "order_by", "sort", "milestone", "view", "labels", "created_after",
        "created_before", "updated_after", "updated_before", "scope", "author", "assignee",
        "my_reaction", "source_branch", "target_branch", "search", "in", "wip"
    };
    QUrl request_url(client->api_endpoint() %
                     "/merge_requests");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * MergeRequests::list_project_mreqs(long project_id, bool fetch_all)
{
    static const QStringList known_params{
        "state", "order_by", "sort", "milestone", "view", "labels", "created_after",
        "created_before", "updated_after", "updated_before", "scope", "author", "assignee",
        "my_reaction", "source_branch", "target_branch", "search", "in", "wip"
    };
    QUrl request_url(client->api_endpoint() %
                     "/projects/" %
                     QString::number(project_id) %
                     "merge_requests");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * MergeRequests::list_group_mreqs(long group_id, bool fetch_all)
{
    static const QStringList known_params{
        "state", "order_by", "sort", "milestone", "view", "labels", "created_after",
        "created_before", "updated_after", "updated_before", "scope", "author", "assignee",
        "my_reaction", "source_branch", "target_branch", "search", "in"
    };
    QUrl request_url(client->api_endpoint() %
                     "/groups/" %
                     QString::number(group_id) %
                     "merge_requests");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObject * MergeRequests::get_mreq(const QString &project_path, long mreq_id)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/merge_requests/" % QString::number(mreq_id));
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * MergeRequests::get_mreq(long project_id, long mreq_id)
{
    return get_mreq(QString::number(project_id), mreq_id);
}

AsyncJsonObject * MergeRequests::set_time_estimate(const QString &project_path,
                                                   long mreq_id, const QString &time_estimate)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/merge_requests/" % QString::number(mreq_id) %
                     "/time_estimate");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->post(request_url, {{"duration", time_estimate}});
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * MergeRequests::set_time_estimate(long project_id,
                                                   long mreq_id, const QString &time_estimate)
{
    return set_time_estimate(QString::number(project_id), mreq_id, time_estimate);
}

AsyncJsonObject * MergeRequests::get_time_stats(const QString &project_path, long mreq_id)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/merge_requests/" % QString::number(mreq_id) %
                     "/time_stats");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response = client->auth_provider()->get(request_url);
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * MergeRequests::get_time_stats(long project_id, long mreq_id)
{
    return get_time_stats(QString::number(project_id), mreq_id);
}

AsyncJsonObject * MergeRequests::add_spent_time(const QString &project_path, long mreq_id,
                                                const QString &spent_time)
{
    QUrl request_url(client->api_endpoint() %
                     "/projects/" % QUrl::toPercentEncoding(project_path) %
                     "/merge_requests/" % QString::number(mreq_id) %
                     "/add_spent_time");

    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->post(request_url, {{"duration", spent_time}});
    return new AsyncJsonObject(response, client);
}

AsyncJsonObject * MergeRequests::add_spent_time(long project_id, long mreq_id,
                                                const QString &spent_time)
{
    return add_spent_time(QString::number(project_id), mreq_id, spent_time);
}
