#include "asyncjsonobject.h"

using namespace gitlabclient;

AsyncJsonObject::AsyncJsonObject(QNetworkReply *reply, Client *client):
    AsyncJsonResponse(reply, client)
{

}

void AsyncJsonObject::fetched(const QJsonDocument &json)
{
    if (!json.isObject()) {
        return;
    }
    fetched_object = json.object();
    emit loaded(this);
}
