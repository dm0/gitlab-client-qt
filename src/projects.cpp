#include <QtNetworkAuth>

#include <QDebug>

#include "projects.h"

using namespace gitlabclient;

Projects::Projects(Client *client):
    QObject(client),
    client(client)
{
}

AsyncJsonObjList * Projects::list_projects(bool fetch_all)
{
    static const QStringList known_params{
        "archived", "visibility", "order_by", "sort", "search", "simple", "owned", "membership",
        "starred", "statistics", "with_custom_attributes", "with_issues_enabled",
        "with_merge_requests_enabled", "wiki_checksum_failed", "repository_checksum_failed",
        "min_access_level"
    };
    QUrl request_url(client->api_endpoint() +
                     "/projects");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Projects::list_user_projects(const QString &user_name, bool fetch_all)
{
    static const QStringList known_params{
        "archived", "visibility", "order_by", "sort", "search", "simple", "owned", "membership",
        "starred", "statistics", "with_custom_attributes", "with_issues_enabled",
        "with_merge_requests_enabled", "min_access_level"
    };
    QUrl request_url(client->api_endpoint() +
                     "/" + QUrl::toPercentEncoding(user_name) + "/projects");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObjList(response, client, fetch_all);
}

AsyncJsonObjList * Projects::list_user_projects(long user_id, bool fetch_all)
{
    return list_user_projects(QString::number(user_id), fetch_all);
}

AsyncJsonObject * Projects::get_project(const QString &project_path)
{
    static const QStringList known_params{
        "statistics", "license", "with_custom_attributes"
    };
    QUrl request_url(client->api_endpoint() +
                     "/" + QUrl::toPercentEncoding(project_path) + "/projects");
    qDebug() << "Sending request to " << request_url;
    QNetworkReply *response =
        client->auth_provider()->get(request_url, params.get_valid(known_params));
    return new AsyncJsonObject(response, client);
}
