#include <QJsonArray>

#include <QDebug>

#include "asyncjsonobjlist.h"

using namespace gitlabclient;

AsyncJsonObjList::AsyncJsonObjList(QNetworkReply *reply, Client *client, bool fetch_all):
    AsyncJsonResponse(reply, client),
    fetch_all_requested(fetch_all)
{
}

std::error_code AsyncJsonObjList::refresh(bool fetch_all)
{
    fetch_all_requested = fetch_all;
    fetched_objects.clear();
    AsyncJsonResponse::refresh();
    return Error::NoError;
}

const QJsonObject & AsyncJsonObjList::at(int i) const
{
    return this->operator[](i);
}

const QJsonObject & AsyncJsonObjList::operator[](int i) const
{
    static const QJsonObject empty;
    if (i >= fetched_objects.size()) {
        return empty;
    }
    return fetched_objects.at(i);
}

std::error_code gitlabclient::AsyncJsonObjList::fetch_all()
{
    fetch_all_requested = true;
    if (pending()) {
        return Error::NoError;
    }
    return fetch_next();
}

void AsyncJsonObjList::fetched(const QJsonDocument &json)
{
    if (!json.isArray()) {
        return;
    }
    const QJsonArray &items = json.array();
    for (int i = 0, len = items.size(); i < len; ++i) {
        const QJsonValue &item = items[i];
        if (item.isObject()) {
            fetched_objects.append(item.toObject());
        }
    }
    if (fetch_all_requested) {
        if (has_more()) {
            fetch_next();
        } else {
            emit loaded(this);
        }
    }

}
