#include <QtNetworkAuth>
#include <QNetworkAccessManager>

#include <QDesktopServices>

#include <QDebug>

#include "gitlabclient.h"
#include "projects.h"
#include "issues.h"
#include "notes.h"
#include "mreqs.h"

using namespace gitlabclient;

Client::Client(QObject *parent):
    QObject(parent),
    gtl_api_endpoint(gtl_instance_url + gtl_api_path),
    netmgr(new QNetworkAccessManager(this)),
    oauth2(new QOAuth2AuthorizationCodeFlow(netmgr, this))
{
    connect(oauth2, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            &QDesktopServices::openUrl);

    connect(oauth2, &QOAuth2AuthorizationCodeFlow::granted, this, &Client::auth_succeed);
    connect(oauth2, &QOAuth2AuthorizationCodeFlow::error, this, &Client::auth_failed);
}

Client::Client(const QString &app_id, const QString &app_secret, QObject *parent):
    Client(parent)
{
    set_app_registration(app_id, app_secret);
}

Client::Client(const QString &token, QObject *parent):
    Client(parent)
{
    set_token(token);
}

QString Client::token() const
{
    return oauth2->token();
}

void Client::set_token(const QString &token)
{
    oauth2->setToken(token);
}

void Client::set_app_registration(const QString &app_id, const QString &app_secret)
{
    gtl_app_id = app_id;
    gtl_app_secret = app_secret;
}

void Client::set_instance_url(const QString &instance_url)
{
    if (instance_url.isEmpty()) {
        gtl_instance_url = "https://gitlab.com";
    } else {
        gtl_instance_url = instance_url;
    }
    gtl_api_endpoint = gtl_instance_url + gtl_api_path;
}

void Client::set_api_path(const QString &path)
{
    gtl_api_path = path;
    gtl_api_endpoint = gtl_instance_url + gtl_api_path;
}

Projects * Client::projects()
{
    if (!shared_projects) {
        shared_projects = new Projects(this);
        connect(shared_projects, &QObject::destroyed,
                this, [=](QObject *) { shared_projects = nullptr; });
    }

    return shared_projects;
}

Issues * gitlabclient::Client::issues()
{
    if (!shared_issues) {
        shared_issues = new Issues(this);
        connect(shared_issues, &QObject::destroyed,
                this, [=](QObject *) { shared_issues = nullptr; });
    }
    return shared_issues;
}

Notes * Client::notes()
{
    if (!shared_notes) {
        shared_notes = new Notes(this);
        connect(shared_notes, &QObject::destroyed,
                this, [=](QObject *) { shared_notes = nullptr; });
    }
    return shared_notes;
}

MergeRequests * Client::merge_requests()
{
    if (!shared_mreqs) {
        shared_mreqs = new MergeRequests(this);
        connect(shared_mreqs, &QObject::destroyed,
                this, [=](QObject *) { shared_mreqs = nullptr; });
    }
    return shared_mreqs;
}

void Client::authorize(QString app_id, QString app_secret)
{
    QOAuthHttpServerReplyHandler *reply_handler = new QOAuthHttpServerReplyHandler(1337, this);

    if (!reply_handler->isListening()) {
        emit auth_failed("port_unavailable",
                         "Failed to setup local HTTP server to listen on port 1337.",
                         QUrl());
        return;
    }

    if (!callback_page.isEmpty()) {
        reply_handler->setCallbackText(callback_page);
    }

    oauth2->setReplyHandler(reply_handler);

    QUrl auth_url(gtl_instance_url);
    auth_url.setPath("/oauth/authorize");
    oauth2->setAuthorizationUrl(auth_url);

    QUrl token_url(gtl_instance_url);
    token_url.setPath("/oauth/token");
    oauth2->setAccessTokenUrl(token_url);

    oauth2->setClientIdentifier(app_id);
    oauth2->setClientIdentifierSharedKey(app_secret);

    oauth2->grant();
}

void Client::authorize()
{
    authorize(gtl_app_id, gtl_app_secret);
}
