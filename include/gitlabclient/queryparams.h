#ifndef GITLABCLIENT_QUERYPARAMS_H
#define GITLABCLIENT_QUERYPARAMS_H

#include <QVariantMap>

namespace gitlabclient {

/**
 * Helper class that store query parameters as QVariantMap
 */
class QueryParams {
public:
    /**
     * Construct QueryParams object.
     */
    QueryParams() = default;

    /**
     * Get parameter value by name.
     * Returns default constructed value in case the parameter is not set.
     * @param name Parameter name
     * @return Parameter value or default constructed value of the type.
     */
    template <typename T>
    T get(const QString &name) const { return params[name].value<T>(); }

    /**
     * Get parameter value by name.
     * Returns `default_value` in case the parameter the parameter is not set.
     * @param name Parameter name
     * @param default_value Default value to return in case the parameter is not set.
     * @return Parameter value or default_value.
     * @overload
     */
    template <typename T>
    T get(const QString &name, T default_value) const
    {
        QVariant value = params.value(name);
        if (value.isValid()) {
            return value.value<T>();
        }
        return default_value;
    }

    /**
     * Reset parameter value.
     * Parameters with not set values do not participate in requests.
     * @param name Parameter name.
     */
    void reset(const QString &name) { params[name].clear(); }

    /**
     * Reset all query parameters (clears internal parameters).
     */
    void reset() { params.clear(); }

    /**
     * Set parameter to the specified value.
     * @param name Parameter name.
     * @param value Parameter value.
     */
    template <typename T>
    void set(const QString &name, const T &value) { params[name] = value; }

    /**
     * Returns a variant map containing only parameters from the list and only set (not default).
     * @param valid_names Allowed names
     * @return Result variant map
     */
    QVariantMap get_valid(const QStringList &valid_names) const;
private:
    QVariantMap params; /**< Parameters store */
};

} // namespace gitlabclient

#endif // GITLABCLIENT_QUERYPARAMS_H
