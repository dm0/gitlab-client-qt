#ifndef GITLABCLIENT_PROJECTS_H
#define GITLABCLIENT_PROJECTS_H

#include <QObject>

#include "gitlabclient.h"
#include "asyncjsonobjlist.h"
#include "asyncjsonobject.h"
#include "queryparams.h"

namespace gitlabclient {

/**
 * Set of API related to projects.
 *
 * Implements the following subset of merge requests API:
 * - List all projects;
 * - List user projects;
 * - Get single project;
 *
 * @see https://docs.gitlab.com/ee/api/notes.html
 */
class Projects: public QObject {
    Q_OBJECT
public:
    /**
     * Construct projects API wrapper.
     * @param client Gitlab client.
     */
    explicit Projects(Client *client);

    /**
     * @name Membership filter
     * The filter limits by projects that the current user is a member of.
     * It can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * Set whether filter enabled or not.
     * @param membership New filter state.
     * @return Current instance reference (enables chaining).
     */
    Projects & membership(bool membership) { params.set("membership", membership); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_membership() { params.reset("membership"); return *this; }
    /**@}*/


    /**
     * @name Archived filter
     * The filter limits by archived status.
     * It can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param archived New filter state.
     * @return Current instance reference (enables chaining).
     */
    Projects & archived(bool archived) { params.set("archived", archived); return *this; }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_archived() { params.reset("archived"); return *this;}
    /**@}*/


    /**
     * @name Visibility filter
     * The filter limits by visibility `public`, `internal`, or `private`.
     * This filter accepts any string but Gitlab will report error in case of unsupported value.
     */
    /**@{*/
    /**
     * Set filter to the specified value.
     * @param visibility New filter value.
     * @return Current instance reference (enables chaining).
     */
    Projects & visibility(const QString &visibility)
    {
        params.set("visibility", visibility);
        return *this;
    }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_visibility() { params.reset("visibility"); return *this; }
    /**@}*/

    /**
     * @name Order by
     * Return projects ordered by id, name, path, created_at, updated_at, or last_activity_at
     * fields. Default is created_at.
     * This class accepts any string but Gitlab will report error in case of unsupported value.
     */
    /**@{*/
    /**
     * Set projects to be ordered by the specified field.
     * @param order_by Field name to order projects by.
     * @return Current instance reference (enables chaining).
     */
    Projects & order_by(const QString &order_by) { params.set("order_by", order_by); return *this; }
    /**
     * Reset projects ordering to Gitlab default.
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_order_by() { params.reset("order_by"); return *this; }
    /**@}*/

    /**
     * @name Sort order
     * Return projects sorted in asc or desc order. Default is desc.
     * This class accepts any string but Gitlab will report error in case of unsupported value.
     */
    /**@{*/
    /**
     * Set sort order. While this function accepts any value, Gitlab will report an error in case
     * of unsupported value.
     * @param sort Sort order of the returned list.
     * @return Current instance reference (enables chaining).
     */
    Projects & sort(const QString &sort) { params.set("sort", sort); return *this; }
    /**
     * Reset sort order to Gitlab default.
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_sort() { params.reset("sort"); return *this; }
    /**@}*/

    /**
     * @name Search projects
     * Return list of projects matching the search criteria.
     * This class accepts any string but Gitlab will report error in case of unsupported value.
     */
    /**@{*/
    /**
     * Set search term.
     * @param search Search term.
     * @return Current instance reference (enables chaining).
     */
    Projects & search(const QString &search) { params.set("search", search); return *this; }
    /**
     * Reset search criterion (disable search).
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_search() { params.reset("search"); return *this; }
    /**@}*/

    /**
     * @name Limit returned list of fields
     * Return only limited fields for each project. This is a no-op without authentication as then
     * only simple fields are returned.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * Set option enabled (true) or disabled (false).
     * @param simple New option state.
     * @return Current instance reference (enables chaining).
     */
    Projects & simple_response(bool simple) { params.set("simple", simple); return *this; }
    /**
     * Reset option to Gitlab default value.
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_simple_response() { params.reset("simple"); return *this; }
    /**@}*/

    /**
     * @name Ownership filter
     * Limit by projects explicitly owned by the current user
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param owned New filter value.
     * @return Current instance reference (enables chaining).
     */
    Projects & owned(bool owned) { params.set("owned", owned); return *this; }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_owned() { params.reset("owned"); return *this; }
    /**@}*/

    /**
     * @name Starred filter
     * Limit by projects starred by the current user.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param starred New filter value
     * @return Current instance reference (enables chaining).
     */
    Projects & starred(bool starred) { params.set("starred", starred); return *this; }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_starred() { params.reset("starred"); return *this; }
    /**@}*/

    /**
     * @name Project statistics
     * Include project statistics.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief simple_response
     * @param include New option value.
     * @return Current instance reference (enables chaining).
     */
    Projects & include_stats(bool include) { params.set("statistics", include); return *this; }
    /**
     * @copydoc reset_simple_response
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_include_stats() { params.reset("statistics"); return *this; }
    /**@}*/

    /**
     * @name Custom attributes
     * Include custom attributes in response (admins only).
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief simple_response
     * @param with_custom_attributes New option value
     * @return Current instance reference (enables chaining).
     */
    Projects & with_custom_attributes(bool with_custom_attributes)
    {
        params.set("with_custom_attributes", with_custom_attributes);
        return *this;
    }
    /**
     * @copydoc reset_simple_response
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_with_custom_attributes()
    {
        params.reset("with_custom_attributes");
        return *this;
    }
    /**@}*/

    /**
     * @name Issues filter
     * Limit by enabled issues feature.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param with_issues_enabled New filter value
     * @return Current instance reference (enables chaining).
     */
    Projects & with_issues_enabled(bool with_issues_enabled)
    {
        params.set("with_issues_enabled", with_issues_enabled);
        return *this;
    }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_with_issues_enabled() { params.reset("with_issues_enabled"); return *this; }
    /**@}*/

    /**
     * @name Enabled merge requests filter
     * Limit by enabled merge requests feature.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param with_merge_requests_enabled New filter value
     * @return Current instance reference (enables chaining).
     */
    Projects & with_merge_requests_enabled(bool with_merge_requests_enabled)
    {
        params.set("with_merge_requests_enabled", with_merge_requests_enabled);
        return *this;
    }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_with_merge_requests_enabled()
    {
        params.reset("with_merge_requests_enabled");
        return *this;
    }
    /**@}*/

    /**
     * @name Failed wiki checksum filter
     * Limit projects where the wiki checksum calculation has failed (Introduced in GitLab
     * Premium 11.2).
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param wiki_checksum_failed New filter value
     * @return Current instance reference (enables chaining).
     */
    Projects & set_wiki_checksum_failed(bool wiki_checksum_failed)
    {
        params.set("wiki_checksum_failed", wiki_checksum_failed);
        return *this;
    }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_wiki_checksum_failed() { params.reset("wiki_checksum_failed"); return *this; }
    /**@}*/

    /**
     * @name Failed repository checksum filter
     * Limit projects where the repository checksum calculation has failed (Introduced in GitLab
     * Premium 11.2).
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief membership
     * @param repository_checksum_failed New filter value
     * @return Current instance reference (enables chaining).
     */
    Projects & set_repository_checksum_failed(bool repository_checksum_failed)
    {
        params.set("repository_checksum_failed", repository_checksum_failed);
        return *this;
    }
    /**
     * @copydoc reset_membership
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_repository_checksum_failed()
    {
        params.reset("repository_checksum_failed");
        return *this;
    }
    /**@}*/

    /**
     * @name Minimal repository access level
     * Limit by current user minimal access level.
     * Gitlab recognizes the following access levels:
     * * `10` → Guest access
     * * `20` → Reporter access
     * * `30` → Developer access
     * * `40` → Maintainer access
     * * `50` → Owner access # Only valid for groups
     * Can be negative if not set (uses Gitlab default).
     */
    /**@{*/
    /**
     * Set minimum access level filter value.
     * This function accepts any value but Gitlab will report an error in case of wrong value.
     * @param min_access_level Minimum access level of the projects to return.
     * @return Current instance reference (enables chaining).
     * @return Current instance reference (enables chaining).
     */
    Projects & min_access_level(int8_t min_access_level)
    {
        params.set("min_access_level", min_access_level);
        return *this;
    }
    /**
     * Reset minimum access level filter.
     * @return Current instance reference (enables chaining).
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_min_access_level() { params.reset("min_access_level"); return *this; }
    /**@}*/

    /**
     * @name Project license
     * Include project license data.
     * This option can be turned on, turned off or not set (uses Gitlab default value).
     */
    /**@{*/
    /**
     * @copybrief simple_response
     * @param include_license New option value.
     * @return Current instance reference (enables chaining).
     */
    Projects & include_license(bool include_license)
    {
        params.set("license", include_license);
        return *this;
    }
    /**
     * @copydoc reset_simple_response
     * @return Current instance reference (enables chaining).
     */
    Projects & reset_include_license() { params.reset("license"); return *this; }
    /**@}*/

    /**
     * Reset all parameters to their defaults.
     * @return Current instance reference (enables chaining).
     */
    Projects & reset() { params.reset(); return *this; }


    /**
     * List all visible projects.
     * Get a list of all visible projects across GitLab for the authenticated user.
     * When accessed without authentication, only public projects with “simple” fields are returned.
     *
     * The results of this method are affected by the following filters and options:
     * `archived`, `visibility`, `order_by`, `sort`, `search`, `simple`, `owned`, `membership`,
     * `starred`, `statistics`, `with_custom_attributes`, `with_issues_enabled`,
     * `with_merge_requests_enabled`, `wiki_checksum_failed`, `repository_checksum_failed`,
     * `min_access_level`.
     *
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects matching search options and filters.
     *
     * @see set_simple_response
     */
    AsyncJsonObjList * list_projects(bool fetch_all=true);

    /**
     * List user projects.
     * Get a list of visible projects owned by the given user. When accessed without authentication,
     * only public projects are returned.
     *
     * The results of this method are affected by the following filters and options:
     * `archived`, `visibility`, `order_by`, `sort`, `search`, `simple`, `owned`, `membership`,
     * `starred`, `statistics`, `with_custom_attributes`, `with_issues_enabled`,
     * `with_merge_requests_enabled`, `min_access_level`.
     * @param user_name The username of the user
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects matching search options and filters.
     */
    AsyncJsonObjList * list_user_projects(const QString &user_name, bool fetch_all=true);

    /**
     * List user projects.
     * Get a list of visible projects owned by the given user. When accessed without authentication,
     * only public projects are returned.
     *
     * The results of this method are affected by the following filters and options:
     * `archived`, `visibility`, `order_by`, `sort`, `search`, `simple`, `owned`, `membership`,
     * `starred`, `statistics`, `with_custom_attributes`, `with_issues_enabled`,
     * `with_merge_requests_enabled`, `min_access_level`.
     * @param user_id The ID of the user
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects matching search options and filters.
     * @overload
     */
    AsyncJsonObjList * list_user_projects(long user_id, bool fetch_all=true);

    /**
     * Get single project.
     * Get a specific project. This endpoint can be accessed without authentication if the project
     * is publicly accessible.
     *
     * The result of this method is affected by the following options:
     * `statistics`, `license`, `with_custom_attributes`.
     * @param project_path The path of the project
     * @return Asynchronous JSON object
     */
    AsyncJsonObject * get_project(const QString &project_path);

    /**
     * Get single project.
     * Get a specific project. This endpoint can be accessed without authentication if the project
     * is publicly accessible.
     *
     * The result of this method is affected by the following options:
     * `statistics`, `license`, `with_custom_attributes`.
     * @param project_id The ID of the project
     * @return Asynchronous JSON object
     * @overload
     */
    AsyncJsonObject * get_project(long project_id);

private:
    Client *client; /**< Parent Gitlab Client used to perform requests. */
    QueryParams params; /**< Parameters used to perform requests */

};

} // namespace gitlabclient

#endif // GITLABCLIENT_PROJECTS_H
