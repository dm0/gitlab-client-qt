#ifndef GITLABCLIENT_H
#define GITLABCLIENT_H

#include <QObject>

class QNetworkAccessManager;
class QOAuth2AuthorizationCodeFlow;

/** @file
 * @brief Class @ref gitlabclient::Client
 */

/** @namespace gitlabclient
 * The Gitlab Client library for Qt framework.
 */

/**
 * @dir include/gitlabclient
 * @brief Brief description
 * Long description.
 */

namespace gitlabclient {
class Projects;
class Issues;
class Notes;
class MergeRequests;

/**
 * GitLab API client.
 * Implements subset of Gitlab API.
 *
 * @see https://docs.gitlab.com/ee/api/
 */
class Client: public QObject {
    Q_OBJECT
public:

    /**
     * Construct GitLab client with no additional information.
     *
     * It will be required to set either auth token or application id and password.
     * @param parent Parent object.
     */
    explicit Client(QObject *parent=nullptr);

    /**
     * Construct GitLab client with the provided GitLab application ID and secret.
     *
     * These parameters are required to make authorize call (can be passed the call itself).
     *
     * @param app_id Application ID. Can be obtained after the app registered with GitLab.
     * @param app_secret Application secret. Obtained with app ID.
     * @param parent Parent object.
     */
    Client(const QString &app_id, const QString &app_secret, QObject *parent=nullptr);

    /**
     * Construct GitLab client with authentication token.
     * @param token Authentication token;
     * @param parent Description
     */
    Client(const QString &token, QObject *parent=nullptr);

    /**
     * Returns authentication token.
     * @return Auth token.
     */
    QString token() const;

    /**
     * Set authentication token.
     * @param token Auth token.
     */
    void set_token(const QString &token);

    /**
     * Set app registration info.
     *
     * Should be the values obtained after registering application with GitLab.
     *
     * @param app_id Application ID.
     * @param app_secret Application token.
     */
    void set_app_registration(const QString &app_id, const QString &app_secret);

    /**
     * Set GitLab instance URL.
     * @param instance_url GitLab instance URL.
     * @sinceversion{v0_2_0,v0.2.0}
     * Method now handles empty string and uses default instance URL in this case
     * (https://gitlab.com).
     */
    void set_instance_url(const QString &instance_url);

    /**
     * Set GitLab API path (part of URL).
     * @param path API path.
     */
    void set_api_path(const QString &path);

    /**
     * Set HTML of the body of the page displayed as the result of GitLab authorization.
     * @param body Page body (HTML).
     */
    void set_callback_page_body(const QString &body) { callback_page = body; }

    /**
     * Returns QOAuth2AuthorizationCodeFlow pointer.
     * @return QOAuth2AuthorizationCodeFlow pointer.
     */
    QOAuth2AuthorizationCodeFlow * auth_provider() const { return oauth2; }

    /**
     * Returns API endpoint (instance + API path).
     * @return API endpoint.
     */
    const QString & api_endpoint() const { return gtl_api_endpoint; }

    /**
     * Returns GitLab instance URL.
     * @return Instance URL
     * @sinceversion{v0_2_0,v0.2.0}
     * Added `instance_url` method.
     */
    const QString & instance_url() const { return gtl_instance_url; }

    /**
     * Returns projects API wrapper.
     * Returns pointer to a shared instance. While it is safe to delete this pointer, it is
     * not recommended.
     * @return Projects API wrapper.
     */
    Projects * projects();

    /**
     * Returns issues API wrapper.
     * Returns pointer to a shared instance. While it is safe to delete this pointer, it is
     * not recommended.
     * @return Issues API wrapper.
     */
    Issues * issues();

    /**
     * Returns issues API wrapper.
     * Returns pointer to a shared instance. While it is safe to delete this pointer, it is
     * not recommended.
     * @return Issues API wrapper.
     */
    Notes * notes();

    /**
     * Returns merge requests API wrapper.
     * Returns pointer to a shared instance. While it is safe to delete this pointer, it is
     * not recommended.
     * @return Merge requests API wrapper.
     */
    MergeRequests * merge_requests();

signals:
    /**
     * The signal is emitted when the authorization has failed.
     *
     * @see authorize
     */
    void auth_failed(const QString &error, const QString &description, const QUrl &url);

    /**
     * The signal is emitted when the authorization has succeed.
     *
     * @see authorize
     */
    void auth_succeed();

public slots:
    /**
     * Try authorizing application with the provided application ID and secret.
     *
     * The result of successful authorization is authentication token that can be saved and used
     * to perform API calls.
     *
     * @param app_id Application ID.
     * @param app_secret Application secret.
     */
    void authorize(QString app_id, QString app_secret);

    /**
     * Try authorizing application with the application ID and secret passed at construction.
     *
     * The result of successful authorization is authentication token that can be saved and used
     * to perform API calls.
     */
    void authorize();

private:
    QString gtl_app_id; /**< Application ID */
    QString gtl_app_secret; /**< Application secret */
    QString gtl_instance_url = "https://gitlab.com"; /** Instance URL */
    QString gtl_api_path = "/api/v4"; /**< API path */
    QString gtl_api_endpoint; /**< API endpoint: url + path */

    Projects *shared_projects = nullptr; /**< Shared projects wrapper instance */
    Issues *shared_issues = nullptr; /**< Shared issues wrapper instance */
    Notes *shared_notes = nullptr; /**< Shared notes wrapper instance */
    MergeRequests *shared_mreqs = nullptr; /**< Shared notes wrapper instance */

    QNetworkAccessManager *netmgr; /**< Network access manager */
    QOAuth2AuthorizationCodeFlow *oauth2; /**< Authorization flow (provider) */
    QString callback_page; /**< Callback page body */
};
} // namespace gitlabclient

#endif // GITLABCLIENT_H
