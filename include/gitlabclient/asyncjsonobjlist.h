#ifndef GITLABCLIENT_ASYNCJSONLIST_H
#define GITLABCLIENT_ASYNCJSONLIST_H

#include <QObject>
#include <QJsonObject>
#include <QNetworkReply>

#include "asyncjsonresponse.h"

namespace gitlabclient {
/**
 * JSON response containing an array of objects.
 */
class AsyncJsonObjList: public AsyncJsonResponse {
    Q_OBJECT
public:
    /**
     * Construct AsyncJsonObjList from network reply and Gitlab Client as parent.
     * @param reply Network reply corresponding to the pending response.
     * @param client Parent Gitlab Client.
     * @param fetch_all If all responses should be fetched (via multiple requests).
     */
    AsyncJsonObjList(QNetworkReply *reply, Client *client, bool fetch_all);

    /**
     * Overloaded refresh implementation.
     *
     * Clears list of JSON objects and calls base implementation.
     * @param fetch_all True to fetch all available results (or only first).
     * @return Error code.
     */
    std::error_code refresh(bool fetch_all=false);

    /**
     * Provides read-only access to fetched JSON objects.
     *
     * Returns object at the specified index. May return empty JSON object if the requested index
     * is out of bounds or not fetched yet.
     * @param i Object index.
     * @return Fetched JSON object or empty JSON object (if not fetched or out of bounds).
     */
    const QJsonObject & at(int i) const;

    /**
     * Provides read-only access to fetched JSON objects.
     *
     * The same as the method `at(int i)`.
     * @param i Object index.
     * @return Fetched JSON object or empty JSON object (if not fetched or out of bounds).
     */
    const QJsonObject & operator[](int i) const;

    /**
     * Returns number of results fetched so far.
     * @return Number of fetched results.
     */
    int fetched_results() const { return fetched_objects.size(); }

    /**
     * Initiate fetching of all available results.
     *
     * This function can be called with request pending.
     *
     * @return Error code
     */
    std::error_code fetch_all();

signals:
    /**
     * The signal is emitted when all the results are fetched.
     */
    void loaded(AsyncJsonObjList *json_objects);

protected:
    /**
     * Overloaded implementation that appends fetched objects to the local list.
     * @param json JSON document.
     */
    void fetched(const QJsonDocument &json) override;

private:
    bool fetch_all_requested; /**< Flag if it was requested to fetch all results. */
    QList<QJsonObject> fetched_objects; /**< JSON objects fetched so far. */
};
}
#endif // GITLABCLIENT_ASYNCJSONLIST_H
