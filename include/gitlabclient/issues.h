#ifndef GITLABCLIENT_ISSUES_H
#define GITLABCLIENT_ISSUES_H

#include <QObject>

#include "gitlabclient.h"
#include "asyncjsonobjlist.h"
#include "asyncjsonobject.h"
#include "queryparams.h"

namespace gitlabclient {

/**
 * Set of API related to issues.
 *
 * Implements the following subset of merge requests API:
 * - List issues;
 * - List group issues;
 * - List project issues;
 * - Get single issue;
 * - Set a time estimate for an issue;
 * - Add spent time for an issue;
 * - Get time tracking stats.
 *
 * @see https://docs.gitlab.com/ee/api/issues.html
 */
class Issues: public QObject {
    Q_OBJECT
public:
    /**
     * Construct issues wrapper.
     * @param client Parent Gitlab Client
     */
    explicit Issues(Client *client);

    /**
     * @name Issue state filter
     * The filter limits issues by the issue state.
     * It takes values `opened`, `closed` or can be disabled (to not filter).
     */
    /**@{*/
    /**
     * Set filter value.
     * @param state New filter value.
     * @return Current instance reference (enables chaining).
     */
    Issues & state(const QString &state) { params.set("state", state); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_state() { params.reset("state"); return *this; }
    /**@}*/

    /**
     * @name Issue labels filter
     * The filter limits issues by their labels.
     * Accepts list of label names, issues must have all labels to be returned.
     * - `None` lists all issues with no labels.
     * - `Any` lists all issues with at least one label.
     * - `No+Label` (Deprecated) lists all issues with no labels.
     * Predefined names are case-insensitive.
     * Empty list means the filter is not set.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param labels Return only issue with this labels.
     * @return Current instance reference (enables chaining).
     */
    Issues & labels(const QStringList &labels) { params.set("labels", labels); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_labels() { params.reset("labels"); return *this; }
    /**@}*/

    /**
     * @name Milestone filter
     * The filter limits issues by their milestone.
     * Accepts milestone title.
     * - `None` lists all issues with no milestone.
     * - `Any` lists all issues that have an assigned milestone.
     * Null string means the filter is not enabled.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param milestone Milestone to filter issues by.
     * @return Current instance reference (enables chaining).
     */
    Issues & milestone(const QString &milestone)
    {
        params.set("milestone", milestone);
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_milestone() { params.reset("milestone"); return *this; }
    /**@}*/

    /**
     * @name Scope filter
     * The filter limits issues by the scope. Allowed values:
     * - created_by_me
     * - assigned_to_me
     * - all.
     * Defaults to created_by_me
     * This implementation accepts any value but Gitlab will report an error in case of unsupported
     * value.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param scope Scope of the issues.
     * @return Current instance reference (enables chaining).
     */
    Issues & scope(const QString &scope) { params.set("scope", scope); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_scope() { params.reset("scope"); return *this; }
    /**@}*/

    /**
     * @name Issue author filter
     * The filter limits issues by their author.
     * Combine with scope=all or scope=assigned_to_me.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param author User Id of the issues' author.
     * @return Current instance reference (enables chaining).
     */
    Issues & author(long author)
    {
        params.set<long long>("author_id", author);
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_author() { params.reset("author_id"); return *this; }
    /**@}*/

    /**
     * @name Issue assignee filter
     * The filter limits issues by the assignee.
     * Negative value corresponds to disabled filter.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param assignee User Id of the assignee.
     * @return Current instance reference (enables chaining).
     */
    Issues & assignee(long assignee)
    {
        params.set<long long>("assignee_id", assignee);
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_assignee() { params.reset("assignee_id"); return *this; }
    /**@}*/

    /**
     * @name Reaction filter
     * The filter limits issues by the reaction emoji of the authenticated user.
     * Accepts emoji string.
     * - `None` returns issues not given a reaction.
     * - `Any` returns issues given at least one reaction.
     * Null string corresponds to the disabled filter.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param reaction Reaction emoji to filter issues by.
     * @return Current instance reference (enables chaining).
     */
    Issues & reaction(const QString &reaction)
    {
        params.set("my_reaction_emoji", reaction);
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_reaction() { params.reset("my_reaction_emoji"); return *this; }
    /**@}*/

    /**
     * @name Weight filter
     * Return issues with the specified weight.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param weight Issues weight.
     * @return Current instance reference (enables chaining).
     */
    Issues & weight(long weight) { params.set<long long>("weight", weight); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_weight() { params.reset("weight"); return *this; }
    /**@}*/

    /**
     * @name Issues sorting
     * Return issues ordered by created_at or updated_at fields. Default is created_at.
     */
    /**@{*/
    /**
     * Set field to order issues by.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param order_by Field name to order issues by.
     * @return Current instance reference (enables chaining).
     */
    Issues & order_by(const QString &order_by) { params.set("order_by", order_by); return *this; }
    /**
     * Reset order by field.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_order_by() { params.reset("order_by"); return *this; }
    /**@}*/

    /**
     * @name Sort order
     * Return issues sorted in asc or desc order. Default is desc.
     */
    /**@{*/
    /**
     * Set issues sort order.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param sort Field name to order issues by.
     * @return Current instance reference (enables chaining).
     */
    Issues & sort(const QString &sort) { params.set("sort", sort); return *this; }
    /**
     * Reset sort order.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_sort() { params.reset("sort"); return *this; }
    /**@}*/

    /**
     * @name Search issues
     * Search issues against their title and/or description.
     */
    /**@{*/
    /**
     * Set search term.
     * @param search Search issues for the passed string.
     * @return Current instance reference (enables chaining).
     */
    Issues & search(const QString &search) { params.set("search", search); return *this; }
    /**
     * Reset search term.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_search() { params.reset("search"); return *this; }
    /**@}*/

    /**
     * @name Search scope
     * Modify the scope of the search attribute. Can be `title`, `description`, or a string
     * joining them with comma. Default is `title,description`.
     */
    /**@{*/
    /**
     * Set search scope.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param scope Search scope.
     * @return Current instance reference (enables chaining).
     */
    Issues & search_scope(const QString &scope) { params.set("in", scope); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_search_scope() { params.reset("in"); return *this; }
    /**@}*/

    /**
     * @name "Created after" filter
     * Return issues created on or after the given time.
     */
    /**@{*/
    /**
     * Set "created after" filter.
     * @param created_after Date-time string to return issues created after or on the given time.
     * @return Current instance reference (enables chaining).
     */
    Issues & created_after(const QString &created_after)
    {
        params.set("created_after", created_after);
        return *this;
    }
    /**
     * Set "created after" filter.
     * @param created_after Date-time to return issues created after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @sinceversion{v0_2_1,v0.2.1} An invalid QDateTime object resets filter.
     *
     * @overload
     */
    Issues & created_after(const QDateTime &created_after)
    {
        if (created_after.isValid()) {
            params.set("created_after", created_after.toString(Qt::DateFormat::ISODateWithMs));
        } else {
            params.reset("created_after");
        }
        params.set("created_after", created_after.toString(Qt::DateFormat::ISODateWithMs));
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_created_after() { params.reset("created_after"); return *this; }
    /**@}*/

    /**
     * @name "Created before" filter
     * Return issues created on or before the given time.
     */
    /**@{*/
    /**
     * Set "created before" filter.
     * @param created_before Date-time string to return issues created before or on the given time.
     * @return Current instance reference (enables chaining).
     */
    Issues & created_before(const QString &created_before)
    {
        params.set("created_after", created_before);
        return *this;
    }
    /**
     * Set "created before" filter.
     * @param created_before Date-time to return issues created after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @sinceversion{v0_2_1,v0.2.1} An invalid QDateTime object resets filter.
     *
     * @overload
     */
    Issues & created_before(const QDateTime &created_before)
    {
        if (created_before.isValid()) {
            params.set("created_before", created_before.toString(Qt::DateFormat::ISODateWithMs));
        } else {
            params.reset("created_before");
        }
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_created_before() { params.reset("created_before"); return *this; }
    /**@}*/

    /**
     * @name "Updated after" filter
     * Return issues updated on or after the given time.
     */
    /**@{*/
    /**
     * Set "updated after" filter.
     * @param updated_after Date-time string to return issues updated after or on the given time.
     * @return Current instance reference (enables chaining).
     */
    Issues & updated_after(const QString &updated_after)
    {
        params.set("updated_after", updated_after);
        return *this;
    }
    /**
     * Set "updated after" filter.
     * @param updated_after Date-time to return issues updated after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @sinceversion{v0_2_1,v0.2.1} An invalid QDateTime object resets filter.
     *
     * @overload
     */
    Issues & updated_after(const QDateTime &updated_after)
    {
        if (updated_after.isValid()) {
            params.set("updated_after", updated_after.toString(Qt::DateFormat::ISODateWithMs));
        } else {
            params.reset("updated_after");
        }
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_updated_after() { params.reset("updated_after"); return *this; }
    /**@}*/

    /**
     * @name "Updated before" filter
     * Return issues updated on or before the given time.
     */
    /**@{*/
    /**
     * Set "updated before" filter.
     * @param updated_before Date-time string to return issues updated before or on the given time.
     * @return Current instance reference (enables chaining).
     */
    Issues & updated_before(const QString &updated_before)
    {
        params.set("updated_after", updated_before);
        return *this;
    }
    /**
     * Set "updated before" filter.
     * @param updated_before Date-time to return issues updated after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @sinceversion{v0_2_1,v0.2.1} An invalid QDateTime object resets filter.
     *
     * @overload
     */
    Issues & updated_before(const QDateTime &updated_before)
    {
        if (updated_before.isValid()) {
            params.set("updated_before", updated_before.toString(Qt::DateFormat::ISODateWithMs));
        } else {
            params.reset("updated_before");
        }
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset_updated_before() { params.reset("updated_before"); return *this; }
    /**@}*/

    /**
     * Reset all parameters to their defaults.
     * @return Current instance reference (enables chaining).
     */
    Issues & reset() { params.reset(); return *this; }

    /**
     * List issues.
     * Get all issues the authenticated user has access to.
     * By default it returns only issues created by the current user.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `labels`, `milestone`, `scope`, `author_id`, `assignee_id`, `my_reaction_emoji`,
     * `weight`, `order_by`, `sort`, `search`, `in` (search scope), `created_after`,
     * `created_before`, `updated_after`, `updated_before`.
     *
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     *
     * @return Asynchronous list of JSON objects with information about issues.
     */
    AsyncJsonObjList * list_issues(bool fetch_all=true);

    /**
     * List group issues.
     * Get a list of a group’s issues.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `labels`, `milestone`, `scope`, `author_id`, `assignee_id`, `my_reaction_emoji`,
     * `weight`, `order_by`, `sort`, `search`, `in` (search scope), `created_after`,
     * `created_before`, `updated_after`, `updated_before`.
     *
     * @param path The path of the group owned by the authenticated user.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about issues.
     */
    AsyncJsonObjList * list_group_issues(const QString &path, bool fetch_all=true);

    /**
     * List group issues.
     * Get a list of a group’s issues.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `labels`, `milestone`, `scope`, `author_id`, `assignee_id`, `my_reaction_emoji`,
     * `weight`, `order_by`, `sort`, `search`, `in` (search scope), `created_after`,
     * `created_before`, `updated_after`, `updated_before`.
     *
     * @param id The ID of the group owned by the authenticated user.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about issues.
     * @overload
     */
    AsyncJsonObjList * list_group_issues(long id, bool fetch_all=true);

    /**
     * List project issues.
     * Get a list of a project’s issues.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `labels`, `milestone`, `scope`, `author_id`, `assignee_id`, `my_reaction_emoji`,
     * `weight`, `order_by`, `sort`, `search`, `in` (search scope), `created_after`,
     * `created_before`, `updated_after`, `updated_before`.
     *
     * @param path The path of the project owned by the authenticated user.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about issues.
     */
    AsyncJsonObjList * list_project_issues(const QString &path, bool fetch_all=true);

    /**
     * List project issues.
     * Get a list of a project’s issues.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `labels`, `milestone`, `scope`, `author_id`, `assignee_id`, `my_reaction_emoji`,
     * `weight`, `order_by`, `sort`, `search`, `in` (search scope), `created_after`,
     * `created_before`, `updated_after`, `updated_before`.
     *
     * @param id The ID of the project owned by the authenticated user.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about issues.
     * @overload
     */
    AsyncJsonObjList * list_project_issues(long id, bool fetch_all=true);

    /**
     * Get a single issue.
     * Get a single issue of the project specified by its path.
     * @param project_path The path of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @return Asynchronous JSON object with issue details.
     */
    AsyncJsonObject * get_issue(const QString &project_path, long issue_id);

    /**
     * Get a single issue.
     * Get a single issue of the project specified by its id and issue iid.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @return Asynchronous JSON object with issue details.
     * @overload
     */
    AsyncJsonObject * get_issue(long project_id, long issue_id);

    /**
     * Set a time estimate for an issue.
     * Sets an estimated time of work for an issue of a project.
     * @param project_path The path of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @param time_estimate The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * set_time_estimate(const QString &project_path, long issue_id,
                                        const QString &time_estimate);

    /**
     * Set a time estimate for an issue.
     * Sets an estimated time of work for an issue of a project.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @param time_estimate The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * set_time_estimate(long project_id, long issue_id,
                                        const QString &time_estimate);

    /**
     * Get time tracking stats.
     * @param project_path The path of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * get_time_stats(const QString &project_path, long issue_id);

    /**
     * Get time tracking stats.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * get_time_stats(long project_id, long issue_id);

    /**
     * Add spent time for an issue.
     * Spent time is a string in human format and can be negative to subtract time.
     * @param project_path The path of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @param spent_time The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * add_spent_time(const QString &project_path, long issue_id,
                                     const QString &spent_time);

    /**
     * Add spent time for an issue.
     * Spent time is a string in human format and can be negative to subtract time.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param issue_id The internal ID of a project’s issue.
     * @param spent_time The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * add_spent_time(long project_id, long issue_id,
                                     const QString &spent_time);


private:
    Client *client; /**< Parent Gitlab Client used to perform requests. */
    QueryParams params; /**< Parameters used to perform requests */
};
} // namespace gitlabclient
#endif // GITLABCLIENT_ISSUES_H
