#ifndef GITLABCLIENT_MREQS_H
#define GITLABCLIENT_MREQS_H

#include <QObject>

#include "gitlabclient.h"
#include "asyncjsonobjlist.h"
#include "asyncjsonobject.h"
#include "queryparams.h"

namespace gitlabclient {

/**
 * Set of API related to merge requests.
 *
 * Implements the following subset of merge requests API:
 * - List merge requests;
 * - List project merge requests;
 * - List group merge requests;
 * - Get single MR;
 * - Set a time estimate for a merge request;
 * - Add spent time for a merge request;
 * - Get time tracking stats.
 *
 * @see https://docs.gitlab.com/ee/api/merge_requests.html
 */
class MergeRequests: public QObject {
    Q_OBJECT
public:
    /**
     * Construct merge requests API wrapper.
     * @param client Parent Gitlab Client
     */
    explicit MergeRequests(Client *client);

    /**
     * @name Merge request state filter
     * The filter limits merge requests by their state.
     * Return all merge requests or just those that are `opened`, `closed`, `locked`, or `merged`.
     */
    /**@{*/
    /**
     * Set filter value.
     * @param state New filter value.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & state(const QString &state) { params.set("state", state); return *this; }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_state() { params.reset("state"); return *this; }
    /**@}*/

    /**
     * @name Merge requests sorting
     * Return requests ordered by `created_at` or `updated_at` fields. Default is `created_at`.
     */
    /**@{*/
    /**
     * Set field to order by.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param order_by Field name to order by.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & order_by(const QString &order_by)
    {
        params.set("order_by", order_by);
        return *this;
    }
    /**
     * Reset order by field.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_order_by() { params.reset("order_by"); return *this; }
    /**@}*/

    /**
     * @name Sort order
     * Return requests sorted in asc or desc order. Default is desc.
     */
    /**@{*/
    /**
     * Set sort order.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param sort Field name to order by.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & sort(const QString &sort) { params.set("sort", sort); return *this; }
    /**
     * Reset sort order.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_sort() { params.reset("sort"); return *this; }
    /**@}*/

    /**
     * @name Milestone filter
     * Return merge requests for a specific milestone.
     * - `None` returns merge requests with no milestone.
     * - `Any` returns merge requests that have an assigned milestone.
     */
    /**@{*/
    /**
     * Set milestone filter.
     * Return merge requests for a specific milestone.
     * - `None` returns merge requests with no milestone.
     * - `Any` returns merge requests that have an assigned milestone.
     * @param milestone Filter value.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & milestone(const QString &milestone)
    {
        params.set("milestone", milestone);
        return *this;
    }
    /**
     * Reset milestone filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_milestone() { params.reset("milestone"); return *this; }
    /**@}*/


    /**
     * @name Merge request response type
     * If `simple`, returns the iid, URL, title, description, and basic state of merge request.
     */
    /**@{*/
    /**
     * Set type of the response.
     * If `simple`, returns the iid, URL, title, description, and basic state of merge request.
     * @param view Response type
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & view(const QString &view) { params.set("view", view); return *this; }
    /**
     * Reset response type.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_view() { params.reset("view"); return *this; }
    /**@}*/


    /**
     * @name Labels filter
     * Return merge requests matching a comma separated list of labels.
     * - `None` lists all merge requests with no labels.
     * - `Any` lists all merge requests with at least one label.
     * - `No+Label` (*Deprecated*) lists all merge requests with no labels.
     * Predefined names are case-insensitive.
     */
    /**@{*/
    /**
     * Set labels filter.
     * Return merge requests matching a comma separated list of labels.
     * - `None` lists all merge requests with no labels.
     * - `Any` lists all merge requests with at least one label.
     * - `No+Label` (*Deprecated*) lists all merge requests with no labels.
     * Predefined names are case-insensitive.
     * @param labels Filter value.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & labels(const QStringList &labels)
    {
        params.set("labels", labels);
        return *this;
    }
    /**
     * Reset labels filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_labels() { params.reset("labels"); return *this; }
    /**@}*/


    /**
     * @name "Created after" filter
     * Return merge requests created on or after the given time.
     */
    /**@{*/
    /**
     * Set "created after" filter.
     * @param created_after Date-time string to return MRs created after or on the given time.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & created_after(const QString &created_after)
    {
        params.set("created_after", created_after);
        return *this;
    }
    /**
     * Set "created after" filter.
     * @param created_after Date-time to return MRs created after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @overload
     */
    MergeRequests & created_after(const QDateTime &created_after)
    {
        params.set("created_after", created_after.toString(Qt::DateFormat::ISODateWithMs));
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_created_after() { params.reset("created_after"); return *this; }
    /**@}*/

    /**
     * @name "Created before" filter
     * Return merge requests created on or before the given time.
     */
    /**@{*/
    /**
     * Set "created before" filter.
     * @param created_before Date-time string to return MRs created before or on the given time.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & created_before(const QString &created_before)
    {
        params.set("created_after", created_before);
        return *this;
    }
    /**
     * Set "created before" filter.
     * @param created_before Date-time to return MRs created after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @overload
     */
    MergeRequests & created_before(const QDateTime &created_before)
    {
        params.set("created_before", created_before.toString(Qt::DateFormat::ISODateWithMs));
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_created_before() { params.reset("created_before"); return *this; }
    /**@}*/

    /**
     * @name "Updated after" filter
     * Return merge requests updated on or after the given time.
     */
    /**@{*/
    /**
     * Set "updated after" filter.
     * @param updated_after Date-time string to return MRs updated after or on the given time.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & updated_after(const QString &updated_after)
    {
        params.set("updated_after", updated_after);
        return *this;
    }
    /**
     * Set "updated after" filter.
     * @param updated_after Date-time to return MRs updated after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @overload
     */
    MergeRequests & updated_after(const QDateTime &updated_after)
    {
        params.set("updated_after", updated_after.toString(Qt::DateFormat::ISODateWithMs));
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_updated_after() { params.reset("updated_after"); return *this; }
    /**@}*/

    /**
     * @name "Updated before" filter
     * Return merge requests updated on or before the given time.
     */
    /**@{*/
    /**
     * Set "updated before" filter.
     * @param updated_before Date-time string to return MRs updated before or on the given time.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & updated_before(const QString &updated_before)
    {
        params.set("updated_after", updated_before);
        return *this;
    }
    /**
     * Set "updated before" filter.
     * @param updated_before Date-time to return MRs updated after or on the given time.
     * @return Current instance reference (enables chaining).
     *
     * @overload
     */
    MergeRequests & updated_before(const QDateTime &updated_before)
    {
        params.set("updated_before", updated_before.toString(Qt::DateFormat::ISODateWithMs));
        return *this;
    }
    /**
     * Reset filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_updated_before() { params.reset("updated_before"); return *this; }
    /**@}*/


    /**
     * @name Merge request scope filter.
     * Return merge requests for the given scope: `created_by_me`, `assigned_to_me` or all.
     */
    /**@{*/
    /**
     * Set merge request scope.
     * Return merge requests for the given scope: `created_by_me`, `assigned_to_me` or all.
     * For versions before 11.0, use the now deprecated `created-by-me` or `assigned-to-me` scopes
     * instead.
     * @param scope Merge request scope
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & scope(const QString &scope) { params.set("scope", scope); return *this; }
    /**
     * Reset response type.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_scope() { params.reset("scope"); return *this; }
    /**@}*/


    /**
     * @name Author filter.
     * Returns merge requests created by the given user id.
     */
    /**@{*/
    /**
     * Set author filter.
     * Returns merge requests created by the given user id.
     * @param author_id Merge request scope
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & author(long author_id)
    {
        params.set<long long>("author", author_id);
        return *this;
    }
    /**
     * Reset response type.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_author() { params.reset("author"); return *this; }
    /**@}*/


    /**
     * @name Assignee filter.
     * Returns merge requests assigned to the given user id.
     * - `None` returns unassigned merge requests.
     * - `Any` returns merge requests with an assignee.
     */
    /**@{*/
    /**
     * Set assignee filter.
     * Returns merge requests assigned to the given user id.
     * - `None` returns unassigned merge requests.
     * - `Any` returns merge requests with an assignee.
     * @param assignee_id Merge request scope
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & assignee(long assignee_id)
    {
        params.set<long long>("assignee", assignee_id);
        return *this;
    }
    /**
     * Reset response type.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_assignee() { params.reset("author"); return *this; }
    /**@}*/


    /**
     * @name Reaction emoji filter.
     * Return merge requests reacted by the authenticated user by the given emoji.
     * - `None` returns issues not given a reaction.
     * - `Any` returns issues given at least one reaction.
     */
    /**@{*/
    /**
     * Set reaction filter.
     * Return merge requests reacted by the authenticated user by the given emoji.
     * - `None` returns issues not given a reaction.
     * - `Any` returns issues given at least one reaction.
     * @param reaction Emoji reaction
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & my_reaction(const QString &reaction)
    {
        params.set("my_reaction_emoji", reaction);
        return *this;
    }
    /**
     * Reset reaction filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_my_reaction() { params.reset("my_reaction_emoji"); return *this; }
    /**@}*/


    /**
     * @name Source branch filter.
     * Return merge requests with the given source branch.
     */
    /**@{*/
    /**
     * Set source branch filter.
     * Return merge requests with the given source branch.
     * @param branch Branch name
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & source_branch(const QString &branch)
    {
        params.set("source_branch", branch);
        return *this;
    }
    /**
     * Reset source branch filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_source_branch() { params.reset("source_branch"); return *this; }
    /**@}*/


    /**
     * @name Target branch filter.
     * Return merge requests with the given target branch.
     */
    /**@{*/
    /**
     * Set target branch filter.
     * Return merge requests with the given target branch.
     * @param branch Branch name
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & target_branch(const QString &branch)
    {
        params.set("target_branch", branch);
        return *this;
    }
    /**
     * Reset target branch filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_target_branch() { params.reset("target_branch"); return *this; }
    /**@}*/


    /**
     * @name Search merge requests.
     * Search merge requests against their `title` and `description`.
     */
    /**@{*/
    /**
     * Set search term.
     * Search merge requests against their `title` and `description`.
     * @param search Search term
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & search(const QString &search)
    {
        params.set("search", search);
        return *this;
    }
    /**
     * Reset search term.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_search() { params.reset("search"); return *this; }
    /**@}*/


    /**
     * @name Search scope.
     * Modify the scope of the search. `title`, `description`, or a string joining them with comma.
     * Default is `title,description`.
     */
    /**@{*/
    /**
     * Set search scope.
     * Modify the scope of the search. `title`, `description`, or a string joining them with comma.
     * Default is `title,description`.
     *
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param scope Search scope
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & in(const QString &scope)
    {
        params.set("in", scope);
        return *this;
    }
    /**
     * Reset search scope.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_in() { params.reset("in"); return *this; }
    /**@}*/


    /**
     * @name Work-in-progress (WIP) filter.
     * Filter merge requests against their `wip` status.
     * - `true` to return *only* WIP merge requests,
     * - `false` to return *non* WIP merge requests.
     * - Reset filter to return merge request with any WIP status.
     */
    /**@{*/
    /**
     * Set WIP filter.
     * Filter merge requests against their `wip` status.
     * - `true` to return *only* WIP merge requests,
     * - `false` to return *non* WIP merge requests.
     * @param wip New filter state.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & wip(bool wip)
    {
        params.set("wip", wip ? "yes" : "no");
        return *this;
    }
    /**
     * Reset WIP filter.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset_wip() { params.reset("wip"); return *this; }
    /**@}*/


    /**
     * Reset all parameters to their defaults.
     * @return Current instance reference (enables chaining).
     */
    MergeRequests & reset() { params.reset(); return *this; }

    /**
     * Get all merge requests the authenticated user has access to.
     * By default it returns only merge requests created by the current user.
     * To get all merge requests, use parameter `scope("all")`.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `order_by`, `sort`, `milestone`, `view`, `labels`, `created_after`,
     * `created_before`, `updated_after`, `updated_before`, `scope`, `author`, `assignee`,
     * `my_reaction`, `source_branch`, `target_branch`, `search`, `in`, `wip`.
     *
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about merge requests.
     */
    AsyncJsonObjList * list_mreqs(bool fetch_all=true);

    /**
     * List project merge requests.
     * Get all merge requests for this project.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `order_by`, `sort`, `milestone`, `view`, `labels`, `created_after`,
     * `created_before`, `updated_after`, `updated_before`, `scope`, `author`, `assignee`,
     * `my_reaction`, `source_branch`, `target_branch`, `search`, `in`, `wip`.
     *
     * @param project_id The ID of a project.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about merge requests.
     */
    AsyncJsonObjList * list_project_mreqs(long project_id, bool fetch_all=true);

    /**
     * List group merge requests.
     * Get all merge requests for this group and its subgroups.
     *
     * The results of this method are affected by the following filters and options:
     * `state`, `order_by`, `sort`, `milestone`, `view`, `labels`, `created_after`,
     * `created_before`, `updated_after`, `updated_before`, `scope`, `author`, `assignee`,
     * `my_reaction`, `source_branch`, `target_branch`, `search`, `in`.
     *
     * @param group_id The ID of a group.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about merge requests.
     */
    AsyncJsonObjList * list_group_mreqs(long group_id, bool fetch_all=true);

    /**
     * Get single merge request.
     * Returns information about a single merge request.
     * @param project_path The path of the project.
     * @param mreq_id The internal ID of the merge request.
     * @return Asynchronous JSON object with information about note.
     */
    AsyncJsonObject * get_mreq(const QString &project_path, long mreq_id);

    /**
     * Get single merge request.
     * Returns information about a single merge request.
     * @param project_id The ID of the project.
     * @param mreq_id The internal ID of the merge request.
     * @return Asynchronous JSON object with information about note.
     * @overload
     */
    AsyncJsonObject * get_mreq(long project_id, long mreq_id);

    /**
     * Set a time estimate for an issue.
     * Sets an estimated time of work for an issue of a project.
     * @param project_path The path of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s MR.
     * @param time_estimate The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * set_time_estimate(const QString &project_path, long mreq_id,
                                        const QString &time_estimate);

    /**
     * Set a time estimate for an issue.
     * Sets an estimated time of work for an issue of a project.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s MR.
     * @param time_estimate The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * set_time_estimate(long project_id, long mreq_id,
                                        const QString &time_estimate);

    /**
     * Get time tracking stats.
     * @param project_path The path of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s MR.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * get_time_stats(const QString &project_path, long mreq_id);

    /**
     * Get time tracking stats.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s MR.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * get_time_stats(long project_id, long mreq_id);

    /**
     * Add spent time for an issue.
     * Spent time is a string in human format and can be negative to subtract time.
     * @param project_path The path of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s issue.
     * @param spent_time The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     */
    AsyncJsonObject * add_spent_time(const QString &project_path, long mreq_id,
                                     const QString &spent_time);

    /**
     * Add spent time for an issue.
     * Spent time is a string in human format and can be negative to subtract time.
     * @param project_id The ID of the project owned by the authenticated user.
     * @param mreq_id The internal ID of a project’s issue.
     * @param spent_time The duration in human format. e.g: 3h30m.
     * @return Asynchronous JSON object with time tracking stats.
     * @overload
     */
    AsyncJsonObject * add_spent_time(long project_id, long mreq_id,
                                     const QString &spent_time);


private:
    Client *client; /**< Parent Gitlab Client used to perform requests. */
    QueryParams params; /**< Parameters used to perform requests */
};
} // namespace gitlabclient


#endif // GITLABCLIENT_MREQS_H
