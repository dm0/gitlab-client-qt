#ifndef GITLABCLIENT_ASYNCJSONOBJECT_H
#define GITLABCLIENT_ASYNCJSONOBJECT_H

#include <QObject>
#include <QJsonObject>
#include <QNetworkReply>

#include "gitlabclient.h"
#include "asyncjsonresponse.h"

namespace gitlabclient {
/**
 * JSON response containing only one object.
 */
class AsyncJsonObject: public AsyncJsonResponse {
    Q_OBJECT
public:
    /**
     * Construct AsyncJsonObject from network reply and Gitlab Client as parent.
     * @param reply Network reply corresponding to the pending response.
     * @param client Parent Gitlab Client
     */
    AsyncJsonObject(QNetworkReply *reply, Client *client);

    /**
     * Returns fetched JSON object.
     * @return Fetched JSON object.
     */
    const QJsonObject & object() const { return fetched_object; }

signals:
    /**
     * Loaded signal emitted after the Json object has loaded.
     * @param json_object Loaded Json object (this instance).
     */
    void loaded(AsyncJsonObject *json_object);

protected:
    /**
     * Overloaded implementation that extracts loaded JSON object from the JSON document.
     * @param json JSON document.
     */
    void fetched(const QJsonDocument &json) override;

private:
    QJsonObject fetched_object; /**< The last fetched JSON object */
};
}
#endif // GITLABCLIENT_ASYNCJSONOBJECT_H
