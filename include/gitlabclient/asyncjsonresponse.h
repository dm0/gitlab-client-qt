#ifndef GITLABCLIENT_ASYNCJSONRESPONSE_H
#define GITLABCLIENT_ASYNCJSONRESPONSE_H

#include <system_error>

#include <QObject>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonDocument>

#include "gitlabclient.h"

namespace gitlabclient {
/**
 * Asynchronous paginated JSON response.
 *
 * The class provides basic facilities for fetching paginated GitLab responses.
 * Only forward navigation (next page) is available.
 */
class AsyncJsonResponse: public QObject {
    Q_OBJECT
public:
    /**
     * Errors returned by the class members;
     */
    enum class Error {
        NoError = 0, /**< No error. */
        Pending,     /**< Another request is pending. */
        NoMorePages  /**< Can't fetch next page -- no more pages.*/
    };

    /**
     * Construct AsyncJsonResponse with the given network reply and Gitlab Client as parent.
     * @param reply Network reply corresponding to the pending request.
     * @param client Parent Gitlab Client.
     */
    AsyncJsonResponse(QNetworkReply *reply, Client *client);

    virtual ~AsyncJsonResponse() = default;

    /**
     * Returns true if can fetch more results.
     * @return Boolean indicating if more results are available.
     */
    bool has_more() const;

    /**
     * Returns last fetched JSON document.
     *
     * This class attempts to parse response text as JSON even in case of error.
     *
     * @return The last fetched JSON document.
     */
    const QJsonDocument & json() const { return json_document; }

    /**
     * @brief Returns error code of the last request.
     * @return The error code of the last request.
     */
    QNetworkReply::NetworkError error() const {return last_error; }

    /**
     * Returns total number of available results.
     * @return The total number of available results.
     */
    int total_results() const { return total_results_cnt; }

    /**
     * Returns total number of pages in the response.
     * @return The total number of pages in the response.
     */
    int total_pages() const { return total_pages_cnt; }

    /**
     * Returns the number of the last loaded page.
     * @return Current page number.
     */
    int current_page() const { return current_page_num; }

    /**
     * Reload request (using the original URL of the first request)
     * @return Error code.
     */
    std::error_code refresh();

    /**
     * Fetch next results page.
     *
     * This function fails if there is already pending request.
     *
     * @return Error code.
     */
    std::error_code fetch_next();

    /**
     * Returns true if there is unfinished request.
     * @return Boolean indicating if there is unfinished request.
     */
    bool pending() const { return has_active_request; }

    /**
     * Returns response time as reported by server.
     * @return QDateTime object with server time of the response.
     */
    QDateTime response_time() const { return when; }

signals:
    /**
     * This signal is emitted when the request has successfully loaded.
     *
     * The payload is total number of pages in the result and the current page number.
     *
     * @param current Current page number.
     * @param total Total number of pages.
     */
    void loaded(int current, int total);

    /**
     * This signal is emitted if the request has failed.
     *
     * The payload carries QNetworkReply::NetworkError code.
     * @param error The error code.
     */
    void failed(QNetworkReply::NetworkError error);

    /**
     * This signal is emitted after the request finished (regardless it failed or succeed).
     *
     * Emitted after loaded or failed, if not failed, error will be NoError.
     * @param error Error happened while handling the request. NoError if completed successfully.
     */
    void finished(QNetworkReply::NetworkError error);

protected slots:
    /**
     * Handle reply's `finished` signal.
     */
    void reply_finished();

    /**
     * Handle reply's `error` signal.
     * @param error
     */
    void reply_error(QNetworkReply::NetworkError error);
protected:
    /**
     * Virtual function that can be overwritten in sub-classes to provide custom logic.
     * Default implementation emits `loaded(current_page, total_pages)` signal if fetch finished
     * without error (`last_error == QNetworkReply::NoError`).
     * @param json Fetched JSON document.
     */
    virtual void fetched(const QJsonDocument &json);

    /**
     * Virtual function that can be overwritten in sub-classes to provide custom logic.
     * Default implementation simply emits `failed(error)` signal.
     * @param error Error occurred during the fetch.
     */
    virtual void fetch_failed(QNetworkReply::NetworkError error);

    /**
     * Connect slots to reply's signals to get notified about reply life cycle.
     * @param reply
     */
    void attach_reply(QNetworkReply *reply);

    QUrl source_url; /**< URL of the original request. Used for refreshing. */
    QJsonDocument json_document; /**< The last fetched JSON document. */
    QString next_page_link; /**< Link to the next page (from the last response). */
    int total_results_cnt; /**< Total number of results (from the last response). */
    int total_pages_cnt; /**< Total number of pages (from the last response). */
    int current_page_num; /**< Current page number (from the last response). */
    Client *client; /**< GitLabClient instance pointer (also parent QObject). */
    QDateTime when = QDateTime::currentDateTime(); /**< Date and time the response was generated */
private:

    /**
     * Reads reply as JSON document, parses headers.
     * @param reply Reply to process.
     */
    void process_reply(QNetworkReply *reply);

    bool has_active_request = true; /**< Flag indicating if there is pending request. */
    QNetworkReply::NetworkError last_error = QNetworkReply::NoError; /**< The last error. */
};

/**
 * AsyncJsonResponse::Error to std::error_code adapter.
 * @param e Error code.
 * @return std::error_code made from the passed AsyncJsonResponse::Error.
 */
std::error_code make_error_code(AsyncJsonResponse::Error e);
} // namespace gitlabclient


namespace std
{
/**
 * Mark AsyncJsonResponse::Error as being error_code.
 */
template <>
struct is_error_code_enum<gitlabclient::AsyncJsonResponse::Error>: true_type {};
}


#endif // GITLABCLIENT_ASYNCJSONRESPONSE_H
