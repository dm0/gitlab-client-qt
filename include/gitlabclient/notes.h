#ifndef GITLABCLIENT_NOTES_H
#define GITLABCLIENT_NOTES_H

#include <QObject>

#include "gitlabclient.h"
#include "asyncjsonobjlist.h"
#include "asyncjsonobject.h"
#include "queryparams.h"

namespace gitlabclient {

/**
 * Set of API related to notes.
 *
 * Implements the following subset of merge requests API:
 * - List project issue notes;
 * - Get single issue note;
 * - List all merge request notes;
 * - Get single merge request note;
 *
 * @see https://docs.gitlab.com/ee/api/notes.html
 */
class Notes: public QObject {
    Q_OBJECT
public:
    /**
     * Construct notes API wrapper.
     * @param client Parent Gitlab Client
     */
    explicit Notes(Client *client);

    /**
     * @name Notes sorting
     * Return notes sorted in asc or desc order. Default is desc.
     */
    /**@{*/
    /**
     * Set field to order by.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param order_by Field name to order by.
     * @return Current instance reference (enables chaining).
     */
    Notes & order_by(const QString &order_by) { params.set("order_by", order_by); return *this; }
    /**
     * Reset order by field.
     * @return Current instance reference (enables chaining).
     */
    Notes & reset_order_by() { params.reset("order_by"); return *this; }
    /**@}*/

    /**
     * @name Sort order
     * Return notes sorted in asc or desc order. Default is desc.
     */
    /**@{*/
    /**
     * Set issues sort order.
     * This function accepts any value but Gitlab will report an error in case of unsupported value.
     * @param sort Field name to order by.
     * @return Current instance reference (enables chaining).
     */
    Notes & sort(const QString &sort) { params.set("sort", sort); return *this; }
    /**
     * Reset sort order.
     * @return Current instance reference (enables chaining).
     */
    Notes & reset_sort() { params.reset("sort"); return *this; }
    /**@}*/


    /**
     * Reset all parameters to their defaults.
     * @return Current instance reference (enables chaining).
     */
    Notes & reset() { params.reset(); return *this; }

    /**
     * List project issue notes.
     * Gets a list of all notes for a single issue.
     *
     * The results of this method are affected by the following options:
     * `order_by` and `sort`.
     *
     * @param project_path The path of the project.
     * @param iid The IID of an issue.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about notes.
     */
    AsyncJsonObjList * list_issue_notes(const QString &project_path, long iid, bool fetch_all=true);

    /**
     * List project issue notes.
     * Gets a list of all notes for a single issue.
     *
     * The results of this method are affected by the following options:
     * `order_by` and `sort`.
     *
     * @param project_id The ID of the project.
     * @param iid The IID of an issue.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about notes.
     * @overload
     */
    AsyncJsonObjList * list_issue_notes(long project_id, long iid, bool fetch_all=true);

    /**
     * Get single issue note.
     * Returns a single note for a specific project issue.
     * @param project_path The path of the project.
     * @param iid The IID of a project issue.
     * @param nid The ID of an issue note.
     * @return Asynchronous JSON object with information about note.
     */
    AsyncJsonObject * get_issue_note(const QString &project_path, long iid, long nid);

    /**
     * Get single issue note.
     * Returns a single note for a specific project issue.
     * @param project_id The ID of the project.
     * @param iid The IID of a project issue.
     * @param nid The ID of an issue note.
     * @return Asynchronous JSON object with information about note.
     * @overload
     */
    AsyncJsonObject * get_issue_note(long project_id, long iid, long nid);

    /**
     * List all merge request notes.
     * Gets a list of all notes for a single merge request.
     *
     * The results of this method are affected by the following options:
     * `order_by` and `sort`.
     *
     * @param project_path The path of the project.
     * @param iid The IID of a project merge request.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about notes.
     */
    AsyncJsonObjList * list_mr_notes(const QString &project_path, long iid, bool fetch_all=true);

    /**
     * List all merge request notes.
     * Gets a list of all notes for a single merge request.
     *
     * The results of this method are affected by the following options:
     * `order_by` and `sort`.
     *
     * @param project_id The ID of the project.
     * @param iid The IID of a project merge request.
     * @param fetch_all true if all responses should be fetched (in contrast to only one page).
     * @return Asynchronous list of JSON objects with information about notes.
     * @overload
     */
    AsyncJsonObjList * list_mr_notes(long project_id, long iid, bool fetch_all=true);

    /**
     * Get single merge request note.
     * Returns a single note for a given merge request.
     * @param project_path The path of the project.
     * @param iid The IID of a project merge request.
     * @param nid The ID of an issue note.
     * @return Asynchronous JSON object with information about note.
     */
    AsyncJsonObject * get_mr_note(const QString &project_path, long iid, long nid);

    /**
     * Get single merge request note.
     * Returns a single note for a given merge request.
     * @param project_id The ID of the project.
     * @param iid The IID of a project merge request.
     * @param nid The ID of an issue note.
     * @return Asynchronous JSON object with information about note.
     * @overload
     */
    AsyncJsonObject * get_mr_note(long project_id, long iid, long nid);


private:
    Client *client; /**< Parent Gitlab Client used to perform requests. */
    QueryParams params; /**< Parameters used to perform requests */
};
} // namespace gitlabclient


#endif // GITLABCLIENT_NOTES_H
